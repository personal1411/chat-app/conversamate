import 'package:conversamate/modals/profile.dart';
import 'package:conversamate/utils/constants.dart';

getUserList(session) async {
  final data = await supabase
      .from('profiles')
      .select('id, username, created_at')
      .neq('id', session?.user.id as Object);
  List<Profile> userList =
      data.map((userMap) => Profile.fromMap(userMap)).toList();
  return userList;
}
