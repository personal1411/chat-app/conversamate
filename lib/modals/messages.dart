class Message {
  final String id;
  final String userId;
  final String otherUserId;
  final String content;
  final DateTime createdAt;
  final bool isMine;
  Message({
    required this.id,
    required this.userId,
    required this.otherUserId,
    required this.content,
    required this.createdAt,
    required this.isMine,
  });

  Message.fromMap({
    required Map<String, dynamic> map,
    required String myUserId,
  })  : id = map['id'],
        userId = map['user_id'],
        otherUserId = map['other_user_id'],
        content = map['content'],
        createdAt = DateTime.parse(map['created_at']),
        isMine = myUserId == map['user_id'];

  Map<String, dynamic> toMap(Iterable<Message> messages) {
    return {
      'id': id,
      'user_id': userId,
      'other_user_id': otherUserId,
      'content': content,
      'created_at': createdAt.toUtc().toIso8601String(),
    };
  }
}
