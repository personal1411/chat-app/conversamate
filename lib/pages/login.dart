import 'package:conversamate/pages/chat.dart';
import 'package:conversamate/pages/user_list.dart';
import 'package:conversamate/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  static Route<void> route() {
    return MaterialPageRoute(builder: (context) => const LoginPage());
  }

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isLoading = false;
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  Future<void> _signIn() async {
    setState(() {
      _isLoading = true;
    });
    try {
      await supabase.auth.signInWithPassword(
        email: _emailController.text,
        password: _passwordController.text,
      );
      final selectedUser =
          await Navigator.of(context).push(UserListPage.route());
      if (selectedUser != null) {
        Navigator.of(context).pushAndRemoveUntil(
          ChatPage.route(selectedUser: selectedUser),
          (route) => true,
        );
      }
    } on AuthException catch (error) {
      context.showErrorSnackBar(message: error.message);
    } catch (_) {
      context.showErrorSnackBar(message: unexpectedErrorMessage);
    } finally {
      if (mounted) {
        setState(() {
          _isLoading = false; // Set to false after authentication
        });
      }
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Sign In')),
      body: Container(
        color: Colors.black,
        child: ListView(
          padding: formPadding,
          children: [
            TextFormField(
              controller: _emailController,
              style: const TextStyle(color: Colors.white),
              decoration: const InputDecoration(
                  label: Text('Email', style: TextStyle(color: Colors.white))),
              keyboardType: TextInputType.emailAddress,
            ),
            formSpacer,
            TextFormField(
              controller: _passwordController,
              style: const TextStyle(color: Colors.white),
              decoration: const InputDecoration(
                  label:
                      Text('Password', style: TextStyle(color: Colors.white))),
              obscureText: true,
            ),
            formSpacer,
            ElevatedButton(
              onPressed: _isLoading ? null : _signIn,
              child: const Text('Login'),
            ),
          ],
        ),
      ),
    );
  }
}
