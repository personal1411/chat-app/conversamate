import 'package:conversamate/pages/chat.dart';
import 'package:conversamate/pages/register.dart';
import 'package:conversamate/pages/user_list.dart';
import 'package:conversamate/utils/constants.dart';
import 'package:flutter/material.dart';

/// Page to redirect users to the appropriate page depending on the initial auth state
class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  SplashPageState createState() => SplashPageState();
}

class SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    _redirect();
  }

  Future<void> _redirect() async {
    await Future.delayed(Duration.zero);
    final session = supabase.auth.currentSession;
    if (session == null) {
      Navigator.of(context)
          .pushAndRemoveUntil(RegisterPage.route(), (route) => false);
    } else {
      final selectedUser =
          await Navigator.of(context).push(UserListPage.route());
      if (selectedUser != null) {
        Navigator.of(context).pushAndRemoveUntil(
          ChatPage.route(selectedUser: selectedUser),
          (route) => false,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(body: preloader);
  }
}
