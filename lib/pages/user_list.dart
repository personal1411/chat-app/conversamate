import 'package:conversamate/modals/profile.dart';
import 'package:conversamate/utils/constants.dart';
import 'package:conversamate/utils/utilities.dart';
import 'package:flutter/material.dart';

class UserListPage extends StatefulWidget {
  static Route<Profile?> route() {
    return MaterialPageRoute(
      builder: (context) => UserListPage(),
    );
  }

  @override
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  late List<Profile> userList = [];

  @override
  void initState() {
    super.initState();
    _fetchUserList();
  }

  Future<void> _fetchUserList() async {
    try {
      final session = supabase.auth.currentSession;
      userList = await getUserList(session);
      setState(() {});
    } catch (error) {
      // Handle error
      print('Error fetching user list: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
              'User List (${supabase.auth.currentSession?.user.userMetadata?['username']})')),
      body: userList.isEmpty
          ? Center(
              child: CircularProgressIndicator(),
            )
          : userList.isEmpty
              ? Center(
                  child: Text('User list is empty.'),
                )
              : ListView.builder(
                  itemCount: userList.length,
                  itemBuilder: (context, index) {
                    final user = userList[index];
                    return ListTile(
                      title: Text(user.username),
                      onTap: () {
                        Navigator.of(context).pop(user);
                      },
                    );
                  },
                ),
    );
  }
}
