import 'dart:async';

import 'package:conversamate/modals/messages.dart';
import 'package:conversamate/modals/profile.dart';
import 'package:conversamate/pages/user_list.dart';
import 'package:conversamate/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:timeago/timeago.dart';

class ChatPage extends StatefulWidget {
  ChatPage({Key? key, required this.selectedUser}) : super(key: key);

  static Route<void> route({required Profile selectedUser}) {
    return MaterialPageRoute(
      builder: (context) => ChatPage(selectedUser: selectedUser),
    );
  }

  final Profile selectedUser;
  final currentUserId = supabase.auth.currentUser!.id;
  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  late final Stream<List<Message>> _messagesStream;
  final Map<String, Profile> _profileCache = {};

  @override
  void initState() {
    final selectedUserId = widget.selectedUser.id;
    var currentUserId = widget.currentUserId;
    _messagesStream = supabase
        .from('messages')
        .stream(primaryKey: ['id'])
        .order('created_at')
        .map((maps) {
          return maps
              .map((map) => Message.fromMap(map: map, myUserId: currentUserId))
              .toList();
        });

    super.initState();
  }

  Future<void> _loadProfileCache(String userId) async {
    if (_profileCache[userId] != null) {
      return;
    }
    final data =
        await supabase.from('profiles').select().eq('id', userId).single();
    final profile = Profile.fromMap(data);
    setState(() {
      _profileCache[userId] = profile;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Chat'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios, color: Color(0xffcd5ff8)),
            onPressed: () async {
              final selectedUser =
                  await Navigator.of(context).push(UserListPage.route());
              if (selectedUser != null) {
                Navigator.of(context).pushAndRemoveUntil(
                  ChatPage.route(selectedUser: selectedUser),
                  (route) => true,
                );
              }
            },
          ),
        ),
        body: StreamBuilder<List<Message>>(
            stream: _messagesStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                final rawMessages = snapshot.data!;
                var messages = rawMessages.where((message) =>
                    (message.userId == widget.currentUserId &&
                        message.otherUserId == widget.selectedUser.id) ||
                    (message.userId == widget.selectedUser.id &&
                        message.otherUserId == widget.currentUserId));

                return Container(
                  color: Colors.black,
                  child: Column(
                    children: [
                      Expanded(
                        child: messages.isEmpty
                            ? const Center(
                                child: Text(
                                  'Start your conversation now :)',
                                  style: TextStyle(color: Colors.white60),
                                ),
                              )
                            : ListView.builder(
                                reverse: true,
                                itemCount: messages.length,
                                itemBuilder: (context, index) {
                                  final message = messages.elementAt(index);

                                  // Load profile cache
                                  _loadProfileCache(message.userId);
                                  return _ChatBubble(
                                    message: message,
                                    profile: _profileCache[message.userId],
                                  );
                                },
                              ),
                      ),
                      _MessageBar(selectedUser: widget.selectedUser),
                    ],
                  ),
                );
              } else if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                return preloader; // Assuming preloader is a widget for loading state
              }
            }));
  }
}

/// Set of widget that contains TextField and Button to submit message
class _MessageBar extends StatefulWidget {
  final Profile selectedUser;

  const _MessageBar({required this.selectedUser});

  @override
  State<_MessageBar> createState() =>
      _MessageBarState(selectedUser: selectedUser);
}

class _MessageBarState extends State<_MessageBar> {
  late final TextEditingController _textController;
  final Profile selectedUser;

  _MessageBarState({required this.selectedUser});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: appTheme.primaryColor,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  maxLines: null,
                  autofocus: true,
                  controller: _textController,
                  decoration: const InputDecoration(
                    hintStyle: TextStyle(color: Colors.white),
                    hintText: 'Type a message',
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    contentPadding: EdgeInsets.all(8),
                  ),
                ),
              ),
              TextButton(
                onPressed: () => _submitMessage(),
                child:
                    const Text('Send', style: TextStyle(color: Colors.black)),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    _textController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  void _submitMessage() async {
    final text = _textController.text;
    final myUserId = supabase.auth.currentUser!.id;
    if (text.isEmpty) {
      return;
    }
    _textController.clear();
    try {
      await supabase.from('messages').insert({
        'user_id': myUserId,
        'other_user_id': selectedUser.id,
        'content': text,
      });
    } on PostgrestException catch (error) {
      context.showErrorSnackBar(message: error.message);
    } catch (_) {
      context.showErrorSnackBar(message: "$_ . $unexpectedErrorMessage");
    }
  }
}

class _ChatBubble extends StatelessWidget {
  const _ChatBubble({
    Key? key,
    required this.message,
    required this.profile,
  }) : super(key: key);

  final Message message;
  final Profile? profile;

  @override
  Widget build(BuildContext context) {
    List<Widget> chatContents = [
      if (!message.isMine)
        CircleAvatar(
          child: profile == null
              ? preloader
              : Text(profile!.username.substring(0, 2)),
        ),
      const SizedBox(width: 12),
      Flexible(
        child: Container(
          padding: const EdgeInsets.symmetric(
            vertical: 8,
            horizontal: 12,
          ),
          decoration: BoxDecoration(
            color:
                message.isMine ? Theme.of(context).primaryColor : Colors.white,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Text(message.content),
        ),
      ),
      const SizedBox(width: 12),
      Text(
        format(message.createdAt, locale: 'en_short'),
        style: const TextStyle(color: Colors.white),
      ),
      const SizedBox(width: 60),
    ];
    if (message.isMine) {
      chatContents = chatContents.reversed.toList();
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 18),
      child: Row(
        mainAxisAlignment:
            message.isMine ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: chatContents,
      ),
    );
  }
}
