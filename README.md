# ConversaMate

ConversaMate is an innovative Flutter project designed to provide a collaborative and efficient communication workspace. Built with Flutter and integrated with Supabase, ConversaMate offers real-time messaging and serves as a foundation for future features like image sharing and dynamic reactions to enhance communication with friends and colleagues.

## Features

### Real-time Messaging
Engage in seamless and instant communication with real-time messaging capabilities.

## Future Prospects

ConversaMate aims to evolve with additional features in the future:

### Image Sharing
Enable users to effortlessly share images within the conversation, enhancing visual communication and collaboration.

### Dynamic Reactions
Express yourself through dynamic reactions to messages, adding a touch of interactivity. React to messages with emojis or custom reactions to convey emotions, opinions, or acknowledgments without the need.

## Getting Started

This project serves as a foundation for a Flutter application. If you are new to Flutter, consider checking out the following resources:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For detailed guidance on Flutter development, refer to the [online documentation](https://docs.flutter.dev/), offering tutorials, samples, and a complete API reference.

## Project Structure

- **lib/:** Contains the main Flutter application code.
- **assets/:** Store your project assets, such as images.

## Dependencies

- [Flutter](https://flutter.dev/)
- [Supabase](https://supabase.io/)

## How to Run

1. Clone the repository.
2. Navigate to the project directory.
3. Run `flutter pub get` to install dependencies.
4. Connect the app with your Supabase instance by updating the relevant configuration files (.env in this case -> please see .env_sample for reference).
5. Run `flutter run` to launch the application on your preferred device.

Feel free to explore and customize ConversaMate to suit your communication and collaboration needs. If you encounter any issues or have questions, refer to the documentation or seek help from the Flutter community.


## Portfolio

Check out my portfolio [here](https://karim.premieronetech.com) for more of my projects and experiences.
